%define debug_package %{nil}

Name:           kubeclarity
Version:        2.23.3
Release:        1%{?dist}
Summary:        KubeClarity is a tool for detection and management of Software Bill Of Materials (SBOM) and vulnerabilities of container images and filesystems

License:        ASL 2.0
URL:            https://runterrascan.io/
Source0:        https://github.com/openclarity/%{name}/releases/download/v%{version}/%{name}-cli-%{version}-linux-amd64.tar.gz

%description
KubeClarity is a tool for detection and management of 
Software Bill Of Materials (SBOM) and vulnerabilities 
of container images and filesystems

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 %{name}-cli %{buildroot}/usr/bin/kubeclarity

%files
/usr/bin/kubeclarity

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Wed Apr 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.23.0

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.22.0

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.19.0

* Thu May 18 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.18.0

* Mon May 08 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.17.1

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.17.0

* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.12.2

* Wed Dec 14 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.9.0

* Fri Nov 25 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.8.0

* Tue Nov 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.7.1

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.7.0

* Thu Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Inital RPM
